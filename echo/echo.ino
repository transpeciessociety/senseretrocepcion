
/*
 * Autor: @fenixbinario
 * 2018 - www.fenixbinario.com  
 */
#include "chrono.h"
#define trigPin 3
#define echoPin 2
#define echo    4

bool State = false;
bool activar = true;
Chrono reloj;
long duration, distance, actual, vibra;
long minimo = 2;
long maximo = 200;
long espera = 800;
long esperaHasta = 400;
void setup() 
{
  Serial.begin (115200);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(echo,OUTPUT);
}
void loop() 
{
  reloj.Loop();
  
if( activar  )
 {  
  if (distance != actual)
  {
    distance = actual;

    vibra = map(distance,minimo,maximo,2,111);
    //  vibra = 49;
      if (distance >= minimo && distance <= maximo)
      {
       
        Serial.println("");
        Serial.print(distance);
        Serial.println(" cm");
        Serial.println("");
        Serial.print(vibra);
        Serial.println(" bpm");
        reloj.Durante();
      }
      else 
      {

         Serial.print(String(distance) + " . ");
      }
      
  }
  else
  {
    if ( reloj.Hasta(esperaHasta)  )
    {
      digitalWrite(trigPin, LOW);        
      delayMicroseconds(2);              
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(5);           
      digitalWrite(trigPin, LOW);
      duration = pulseIn(echoPin, HIGH);
      actual = (duration/2) / 29.1;
    }
  }
 }  
 if(  reloj.Durante(espera) ) 
   {   
      if ( reloj.Hasta(vibra) )
        {
          if( !State == HIGH )
             {
              State = HIGH;  // ciclon *on*
              //Serial.print ( String(State) );
             }
          else
            {
             State = LOW;   // ciclon *off*
             //Serial.print ( String(State) );
            }
        }
     digitalWrite(echo, State);
     activar = false;
    }
    else
    {
      activar = true;
      digitalWrite(echo, LOW);
      
    }
  
}
