/*
Miembros de la clase de Chrono
*/
#include "chrono.h"


Chrono::Chrono(){}

bool Chrono::begin()
{
  return true;
}

bool Chrono::Hasta(unsigned long espera)
{
   if(time - contadorEspera >= espera) {
     contadorEspera = time;
     return true;
  }
  else{
    return false;
  }
}

void Chrono::Durante(void)
{
  EncendidoDurante = true;
}

bool Chrono::Durante(unsigned long espera)
{
  
  if(time - contadorDurante <= espera && EncendidoDurante == true) 
  {
    return true;
  }
  else
  {
    EncendidoDurante = false;
    contadorDurante = time; 
    return false;
  }
}

void Chrono::Loop()
{
  time = millis();
}

void Chrono::Mostrar(void)
{
  Serial.println( time  );
}

void Chrono::Info(void)
{
  Serial.println( INFO );
}


